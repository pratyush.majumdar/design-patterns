# DESIGN PATTERNS

In software engineering, a software design pattern is a general, reusable solution to a commonly occurring problem. It is a description or template for how to solve a problem that can be used in many different situations.

## Creational Design Pattern
Creational design patterns provide solution to instantiate an object in the best possible way for specific situations. Example
1.	Abstract Factory Pattern
2.	Builder Pattern
3.	Factory Pattern
4.	Singleton Pattern
5.	Prototype Pattern

## Structural Design Pattern
Structural Patterns provide different ways to create a class structure, for example using inheritance and composition to create a large object from small objects.
1.	Adapter Pattern
2.	Bridge Pattern
3.	Composite Pattern
4.	Decorator Pattern
5.	Fa�ade Pattern
6.	Flyweight Pattern
7.	Proxy Pattern

## Behavioral Design Pattern
1.	Command Pattern
2.	Chain of Responsibility Pattern
3.	Interpreter Pattern
4.	Iterator Pattern
5.	Mediator Pattern
6.	Memento Pattern
7.	Template Method
8.	Visitor Pattern
9.	Observer Pattern
10.	Strategy Pattern
11.	State Pattern
