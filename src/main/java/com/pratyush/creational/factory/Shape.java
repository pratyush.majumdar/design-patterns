package com.pratyush.creational.factory;

public interface Shape {
	void draw();
}