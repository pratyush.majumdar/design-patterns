package com.pratyush.creational.singleton;

import java.io.Serializable;

class SuperClass2 implements Cloneable {
	@Override
	protected Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	}
}

public class UnBreakableSingleton extends SuperClass2 implements Serializable {
	// using Cloneable to show breaking of Singleton by Cloning
	// using Serializable to show breaking of Singleton by Serialization
	private static final long serialVersionUID = -6699952257967257825L;	
	
	// Eager Initialization
	public static UnBreakableSingleton singleTon = new UnBreakableSingleton();
	
	private UnBreakableSingleton() {
		
	}
	
	// implement readResolve method to prevent serialization
    protected Object readResolve() { 
        return singleTon; 
    } 
    
 // overide clone method to prevent Cloning
	@Override
	protected Object clone() throws CloneNotSupportedException { 
//	    throw new CloneNotSupportedException();
		return singleTon;
	}
}

