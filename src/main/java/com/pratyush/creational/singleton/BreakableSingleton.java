package com.pratyush.creational.singleton;

import java.io.Serializable;

class SuperClass1 implements Cloneable {
	@Override
	protected Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	}
}

public class BreakableSingleton extends SuperClass1 implements Serializable {
	// using Cloneable to show breaking of Singleton by Cloning
	// using Serializable to show breaking of Singleton by Serialization
	private static final long serialVersionUID = -2153769253207113968L;
	
	private static BreakableSingleton singleTon = null;
	
	// private constructor to force use of getInstance() to create Singleton object 
	private BreakableSingleton() {
		
	}
	
	// Lazy Initialization
	// synchronized keyword so that only one thread can execute this at a time 
	public static synchronized BreakableSingleton getInstance() {
		if(singleTon == null)
			return new BreakableSingleton();
		else
			return singleTon;
	}
}
