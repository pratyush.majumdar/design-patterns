package com.pratyush.creational.singleton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;

public class SingletonPatternDemo2 {
	public static void main(String[] args) {
		SingletonPatternDemo2 singletonPatternDemo = new SingletonPatternDemo2();
		
		//Breaking by reflection
		singletonPatternDemo.breakingByReflection();
		
		//Breaking by serialization
		singletonPatternDemo.breakingBySerialization();
		
		//Breaking by Cloning
		singletonPatternDemo.breakingByCloning();
	}
	
	//TO BE DONE
	private void breakingByReflection() {
		UnBreakableSingleton instance1 = UnBreakableSingleton.singleTon; 
		UnBreakableSingleton instance2 = null;
		try {
			Constructor<?>[] constructors = UnBreakableSingleton.class.getDeclaredConstructors();
			for (Constructor<?> constructor : constructors) {
				constructor.setAccessible(true);
				instance2 = (UnBreakableSingleton) constructor.newInstance();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("instance1.hashCode(): " + instance1.hashCode()); 
		System.out.println("instance2.hashCode(): " + instance2.hashCode());
	}
	
	private void breakingBySerialization() {
		UnBreakableSingleton instance1 = UnBreakableSingleton.singleTon; 
		UnBreakableSingleton instance2 = null;
		try {
			// serialize from object to file
			ObjectOutput out = new ObjectOutputStream(new FileOutputStream("file.text")); 
			out.writeObject(instance1);
			out.close();
			
			// de-serialize from file to object 
            ObjectInput in = new ObjectInputStream(new FileInputStream("file.text"));
            instance2 = (UnBreakableSingleton) in.readObject(); 
            in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("instance1.hashCode(): " + instance1.hashCode()); 
		System.out.println("instance2.hashCode(): " + instance2.hashCode());
	}

	private void breakingByCloning() {
		UnBreakableSingleton instance1 = UnBreakableSingleton.singleTon; 
		UnBreakableSingleton instance2 = null;
		try {
			instance2 = (UnBreakableSingleton) instance1.clone();
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("instance1.hashCode(): " + instance1.hashCode()); 
		System.out.println("instance2.hashCode(): " + instance2.hashCode());
	}
}
