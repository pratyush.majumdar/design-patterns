package com.pratyush.creational.singleton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class SingletonPatternDemo3 {
	public static void main(String[] args) {
		SingletonPatternDemo3 singletonPatternDemo3 = new SingletonPatternDemo3();
		
		singletonPatternDemo3.breakingBySerialization();
	}
	
	private void breakingBySerialization() {
		EnumSingleton instance1 = EnumSingleton.singleTon; 
		EnumSingleton instance2 = null;
		try {
			// serialize from object to file
			ObjectOutput out = new ObjectOutputStream(new FileOutputStream("file.text")); 
			out.writeObject(instance1);
			out.close();
			
			// de-serialize from file to object 
            ObjectInput in = new ObjectInputStream(new FileInputStream("file.text"));
            instance2 = (EnumSingleton) in.readObject(); 
            in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("instance1.hashCode(): " + instance1.hashCode()); 
		System.out.println("instance2.hashCode(): " + instance2.hashCode());
	}
}
