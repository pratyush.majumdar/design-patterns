package com.pratyush.creational.singleton;

import java.io.Serializable;

public enum EnumSingleton implements Serializable {
	singleTon;
	
	// no need for private constructor
	// no need for synchronize keyword, already thread safe
	// clone() method is not available
	// no need for getInstance
}
