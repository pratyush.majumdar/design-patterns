package com.pratyush.behavioral.iterator;

public interface Container {
	   public Iterator getIterator();
	}
