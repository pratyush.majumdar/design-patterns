package com.pratyush.structural.facade;

public interface Shape {
	void draw();
}
