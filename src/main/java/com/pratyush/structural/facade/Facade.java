package com.pratyush.structural.facade;

public class Facade {
	private Shape circle;
	private Shape rectangle;

	public Facade() {
	      circle = new Circle();
	      rectangle = new Rectangle();
	   }

	public void drawCircle() {
		circle.draw();
	}

	public void drawRectangle() {
		rectangle.draw();
	}
}
